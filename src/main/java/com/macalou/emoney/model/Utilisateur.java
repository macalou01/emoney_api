package com.macalou.emoney.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Utilisateur {

    @Id
    @SequenceGenerator(name = "utilisateur_sequence", sequenceName = "utilisateur_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_sequence")
    private long utilisateurId;

    @Column(nullable = false, unique = true)
    private String numeroTel;

    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private String mdp;

    private String prenom;

    @NotBlank
    private String nom;

    private String email;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Sexe sexe;

    private LocalDate dateNais;

    @ManyToOne
    @JoinColumn(name = "fk_profilId", nullable = false)
    @JsonIgnoreProperties("profilRoles")
    private Profil profil;

    @OneToOne
    @JoinColumn(name = "fk_compteId", nullable = false, unique = true, updatable = false)
    @JsonManagedReference(value = "compte-utilisateur")
    @JsonIgnoreProperties("operations")
    private Compte compte;

    @JsonIgnore
    @OneToMany(targetEntity = Historique.class, mappedBy = "effectuerPar", fetch = FetchType.LAZY)
    private List<Historique> historiques = new ArrayList<>();

    public Utilisateur() {
    }

    public Utilisateur(long utilisateurId, String numeroTel, @NotNull String mdp, String prenom, String nom,
            String email, @NotNull Sexe sexe, LocalDate dateNais, Profil profil, Compte compte,
            List<Historique> historiques) {
        this.utilisateurId = utilisateurId;
        this.numeroTel = numeroTel;
        this.mdp = mdp;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.sexe = sexe;
        this.dateNais = dateNais;
        this.profil = profil;
        this.compte = compte;
        this.historiques = historiques;
    }

    public Utilisateur(String numeroTel, @NotNull String mdp, String prenom, String nom, String email,
            @NotNull Sexe sexe, LocalDate dateNais, Profil profil, Compte compte, List<Historique> historiques) {
        this.numeroTel = numeroTel;
        this.mdp = mdp;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.sexe = sexe;
        this.dateNais = dateNais;
        this.profil = profil;
        this.compte = compte;
        this.historiques = historiques;
    }

    public long getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(long utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    public String getNumeroTel() {
        return numeroTel;
    }

    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public LocalDate getDateNais() {
        return dateNais;
    }

    public void setDateNais(LocalDate dateNais) {
        this.dateNais = dateNais;
    }

    public Profil getProfil() {
        return profil;
    }

    public void setProfil(Profil profil) {
        this.profil = profil;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public List<Historique> getHistoriques() {
        return historiques;
    }

    public void setHistoriques(List<Historique> historiques) {
        this.historiques = historiques;
    }

    @Override
    public String toString() {
        return "Utilisateur [dateNais=" + dateNais + ", email=" + email + ", mdp=" + mdp + ", nom=" + nom
                + ", numeroTel=" + numeroTel + ", prenom=" + prenom + ", sexe=" + sexe + ", utilisateurId="
                + utilisateurId + "]";
    }

}