package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.BadRequestException;
import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Depot;
import com.macalou.emoney.model.Operation;
import com.macalou.emoney.model.TypeOperation;
import com.macalou.emoney.repository.DepotRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepotService {
    private final DepotRepository depotRepository;

    @Autowired
    public DepotService(DepotRepository depotRepository) {
        this.depotRepository = depotRepository;
    }

    public void add(Depot depot) {
        if (depot.getTypeOperation() == TypeOperation.DEPOT) {
            depotRepository.save(depot);
        } else {
            throw new BadRequestException("Bad request");
        }
    }

    public List<Depot> findALL() {
        List<Operation> operation = depotRepository.findAll();
        List<Depot> depot = new ArrayList<>();
        if (operation.isEmpty()) {
            throw new ResourceNotFoundException("resource with not found");
        }
        for (Operation operation_ : operation) {
            if (operation_.getTypeOperation() == TypeOperation.DEPOT) {
                depot.add((Depot) operation_);
            }
        }
        return depot;
    }

    public Depot findOne(long depotId) {
        Operation operation = depotRepository.findById(depotId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + depotId + " not found"));
        Depot depot = new Depot();
        if (operation.getTypeOperation() == TypeOperation.DEPOT) {
            depot = (Depot) operation;
        } else {
            throw new ResourceNotFoundException("resource with id " + depotId + " not found");
        }
        return depot;

    }

}