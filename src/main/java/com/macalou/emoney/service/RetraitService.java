package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.BadRequestException;
import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Operation;
import com.macalou.emoney.model.Retrait;
import com.macalou.emoney.model.TypeOperation;
import com.macalou.emoney.repository.RetraitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RetraitService {
    private final RetraitRepository retraitRepository;

    @Autowired
    public RetraitService(RetraitRepository retraitRepository) {
        this.retraitRepository = retraitRepository;
    }

    public void add(Retrait retrait) {
        if (retrait.getTypeOperation() == TypeOperation.RETRAIT) {
            retraitRepository.save(retrait);
        } else {
            throw new BadRequestException("Bad request");
        }
    }

    public List<Retrait> findALL() {
        List<Operation> operation = retraitRepository.findAll();
        List<Retrait> retrait = new ArrayList<>();
        if (operation.isEmpty()) {
            throw new ResourceNotFoundException("resource with not found");
        }
        for (Operation operation_ : operation) {
            if (operation_.getTypeOperation() == TypeOperation.RETRAIT) {
                retrait.add((Retrait) operation_);
            }
        }
        return retrait;
    }

    public Retrait findOne(long retraitId) {
        Operation operation = retraitRepository.findById(retraitId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + retraitId + " not found"));
        Retrait retrait = new Retrait();
        if (operation.getTypeOperation() == TypeOperation.RETRAIT) {
            retrait = (Retrait) operation;
        } else {
            throw new ResourceNotFoundException("resource with id " + retraitId + " not found");
        }
        return retrait;

    }

}