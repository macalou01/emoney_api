package com.macalou.emoney.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Compte {

    @Id
    @SequenceGenerator(name = "compte_sequence", sequenceName = "compte_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "compte_sequence")
    private long compteId;

    @Min(value = 0L, message = "Entrez une valeur positive")
    private long solde;

    @JsonBackReference(value = "compte-utilisateur")
    @OneToOne(mappedBy = "compte")
    private Utilisateur utilisateur;

    @JsonIgnore
    @OneToMany(targetEntity = Operation.class, mappedBy = "compte", fetch = FetchType.LAZY)
    private List<Operation> operations = new ArrayList<>();

    public Compte() {
    }

    public Compte(long compteId, @Min(value = 0, message = "Entrez une valeur positive") long solde,
            Utilisateur utilisateur, List<Operation> operations) {
        this.compteId = compteId;
        this.solde = solde;
        this.utilisateur = utilisateur;
        this.operations = operations;
    }

    public Compte(@Min(value = 0, message = "Entrez une valeur positive") long solde, Utilisateur utilisateur,
            List<Operation> operations) {
        this.solde = solde;
        this.utilisateur = utilisateur;
        this.operations = operations;
    }

    public long getCompteId() {
        return compteId;
    }

    public void setCompteId(long compteId) {
        this.compteId = compteId;
    }

    public long getSolde() {
        return solde;
    }

    public void setSolde(long solde) {
        this.solde = solde;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public String toString() {
        return "Compte [compteId=" + compteId + ", solde=" + solde + "]";
    }

}