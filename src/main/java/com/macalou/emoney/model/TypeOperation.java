package com.macalou.emoney.model;

public enum TypeOperation {
    DEPOT("D"), RETRAIT("R"), TRANSFERT("T");

    final String code;

    TypeOperation(String code) {
        this.code = code;
    }
}