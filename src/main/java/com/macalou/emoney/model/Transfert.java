package com.macalou.emoney.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue(value = "TRANSFERT")
public class Transfert extends Operation {

    @NotNull
    private long numeroDestination;

    public Transfert() {
    }

    public Transfert(long operationId, @NotNull LocalDateTime dateOperation, long montant, Compte compte,
            List<Historique> historiques, TypeOperation typeOperation, long numeroDestination) {
        super(operationId, dateOperation, montant, compte, historiques, typeOperation);
        this.numeroDestination = numeroDestination;
    }

    public Transfert(@NotNull LocalDateTime dateOperation, long montant, Compte compte, List<Historique> historiques,
            TypeOperation typeOperation, long numeroDestination) {
        super(dateOperation, montant, compte, historiques, typeOperation);
        this.numeroDestination = numeroDestination;
    }

    public long getNumeroDestination() {
        return numeroDestination;
    }

    public void setNumeroDestination(long numeroDestination) {
        this.numeroDestination = numeroDestination;
    }

    @Override
    public String toString() {
        return "Transfert [numeroDestination=" + numeroDestination + "]";
    }

}
