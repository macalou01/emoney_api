package com.macalou.emoney.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Profil {

    @Id
    @SequenceGenerator(name = "profil_sequence", sequenceName = "profil_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profil_sequence")
    private int profilId;

    @NotBlank
    private String nomProfil;

    @ManyToMany
    @JoinTable(name = "profil_roles", joinColumns = @JoinColumn(name = "profilId"), inverseJoinColumns = @JoinColumn(name = "roleId"))
    private List<Role> profilRoles = new ArrayList<>();
    
    @JsonIgnore
    @OneToMany(targetEntity = Utilisateur.class, mappedBy = "profil", fetch = FetchType.LAZY)
    private List<Utilisateur> profilUtilisateurs = new ArrayList<>();

    public Profil() {
    }

    public Profil(int profilId, String nomProfil, List<Role> profilRoles, List<Utilisateur> profilUtilisateurs) {
        this.profilId = profilId;
        this.nomProfil = nomProfil;
        this.profilRoles = profilRoles;
        this.profilUtilisateurs = profilUtilisateurs;
    }

    public Profil(String nomProfil, List<Role> profilRoles, List<Utilisateur> profilUtilisateurs) {
        this.nomProfil = nomProfil;
        this.profilRoles = profilRoles;
        this.profilUtilisateurs = profilUtilisateurs;
    }

    public void addRole(Role role) {
        this.profilRoles.add(role);
    }

    public int getProfilId() {
        return profilId;
    }

    public void setProfilId(int profilId) {
        this.profilId = profilId;
    }

    public String getNomProfil() {
        return nomProfil;
    }

    public void setNomProfil(String nomProfil) {
        this.nomProfil = nomProfil;
    }

    public List<Role> getProfilRoles() {
        return profilRoles;
    }

    public void setProfilRoles(List<Role> profilRoles) {
        this.profilRoles = profilRoles;
    }

    public List<Utilisateur> getProfilUtilisateurs() {
        return profilUtilisateurs;
    }

    public void setProfilUtilisateurs(List<Utilisateur> profilUtilisateurs) {
        this.profilUtilisateurs = profilUtilisateurs;
    }

    @Override
    public String toString() {
        return "Profil [nomProfil=" + nomProfil + ", profilId=" + profilId + "]";
    }

}
