package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Retrait;
import com.macalou.emoney.service.RetraitService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/retraits")
public class RetraitController {

   public final RetraitService retraitService;

    @Autowired
    public RetraitController(RetraitService retraitService) {
        this.retraitService = retraitService;
    }

    // **Get All */
    @GetMapping
    public List<Retrait> getRetraits() {
        return retraitService.findALL();
    }

    // **Get One */
  @GetMapping(path = "{id}")
    public Retrait getOne(@PathVariable("id") Long id) {
        return retraitService.findOne(id);
    }

   // **Post 
    @PostMapping
    public void addNewRetrait(@RequestBody Retrait retrait) {
        retraitService.add(retrait);
    }

}

