package com.macalou.emoney.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue(value = "RETRAIT")
public class Retrait extends Operation {

    @NotNull // FIXME: any null pointer exception if null
    private long numeroSource;

    public Retrait() {
    }

    public Retrait(long operationId, @NotNull LocalDateTime dateOperation, long montant, Compte compte,
            List<Historique> historiques, TypeOperation typeOperation, @NotNull long numeroSource) {
        super(operationId, dateOperation, montant, compte, historiques, typeOperation);
        this.numeroSource = numeroSource;
    }

    public Retrait(@NotNull LocalDateTime dateOperation, long montant, Compte compte, List<Historique> historiques,
            TypeOperation typeOperation, @NotNull long numeroSource) {
        super(dateOperation, montant, compte, historiques, typeOperation);
        this.numeroSource = numeroSource;
    }

    public long getNumeroSource() {
        return numeroSource;
    }

    public void setNumeroSource(long numeroSource) {
        this.numeroSource = numeroSource;
    }

    @Override
    public String toString() {
        return "Retrait [numeroSource=" + this.numeroSource + "]";
    }

}
