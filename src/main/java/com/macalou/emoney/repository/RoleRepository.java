package com.macalou.emoney.repository;

import com.macalou.emoney.model.Role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    // ? find role by "nomRole" JPQL statement
//@Query("SELECT s FROM T_role s WHERE s.nom_role =?1")
//Optional<Role> findStudentByEmail(String email);
}
