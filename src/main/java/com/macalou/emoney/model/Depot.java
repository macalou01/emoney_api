package com.macalou.emoney.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue(value = "DEPOT")
public class Depot extends Operation {

    @NotNull // FIXME: any null pointer exception if null
    private long numeroDestination;

    public Depot() {
    }

    public Depot(long operationId, @NotNull LocalDateTime dateOperation, long montant, Compte compte,
            List<Historique> historiques, TypeOperation typeOperation, @NotNull long numeroDestination) {
        super(operationId, dateOperation, montant, compte, historiques, typeOperation);
        this.numeroDestination = numeroDestination;
    }

    public Depot(@NotNull LocalDateTime dateOperation, long montant, Compte compte, List<Historique> historiques,
            TypeOperation typeOperation, @NotNull long numeroDestination) {
        super(dateOperation, montant, compte, historiques, typeOperation);
        this.numeroDestination = numeroDestination;
    }

    public long getNumeroDestination() {
        return numeroDestination;
    }

    public void setNumeroDestination(long numeroDestination) {
        this.numeroDestination = numeroDestination;
    }

    @Override
    public String toString() {
        return "Depot [numeroDestination=" + numeroDestination + "]";
    }

}
