package com.macalou.emoney.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Role {

    @Id
    @SequenceGenerator(name = "role_sequence", sequenceName = "role_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_sequence")
    private int roleId;

    private String nomRole;

    @JsonIgnore
    @ManyToMany(mappedBy = "profilRoles")
    private List<Profil> profiles = new ArrayList<>();

    public Role() {
    }

    public Role(int roleId, String nomRole, List<Profil> profiles) {
        this.roleId = roleId;
        this.nomRole = nomRole;
        this.profiles = profiles;
    }

    public Role(int roleId, String nomRole) {
        this.roleId = roleId;
        this.nomRole = nomRole;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getNomRole() {
        return nomRole;
    }

    public void setNomRole(String nomRole) {
        this.nomRole = nomRole;
    }

    public List<Profil> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profil> profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return "Role [nomRole=" + this.nomRole + ", profiles=" + this.profiles + ", roleId=" + this.roleId + "]";
    }

}
