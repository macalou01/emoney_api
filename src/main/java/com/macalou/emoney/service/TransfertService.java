package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.BadRequestException;
import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Operation;
import com.macalou.emoney.model.Transfert;
import com.macalou.emoney.model.TypeOperation;
import com.macalou.emoney.repository.TransfertRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransfertService {
    private final TransfertRepository transfertRepository;

    @Autowired
    public TransfertService(TransfertRepository transfertRepository) {
        this.transfertRepository = transfertRepository;
    }

    public void add(Transfert transfert) {
        if (transfert.getTypeOperation() == TypeOperation.TRANSFERT) {
            transfertRepository.save(transfert);
        } else {
            throw new BadRequestException("Bad request");
        }
    }

    public List<Transfert> findALL() {
        List<Operation> operation = transfertRepository.findAll();
        List<Transfert> transfert = new ArrayList<>();
        if (operation.isEmpty()) {
            throw new ResourceNotFoundException("resource with not found");
        }
        for (Operation operation_ : operation) {
            if (operation_.getTypeOperation() == TypeOperation.TRANSFERT) {
                transfert.add((Transfert) operation_);
            }
        }
        return transfert;
    }

    public Transfert findOne(long transfertId) {
        Operation operation = transfertRepository.findById(transfertId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + transfertId + " not found"));
        Transfert transfert = new Transfert();
        if (operation.getTypeOperation() == TypeOperation.TRANSFERT) {
            transfert = (Transfert) operation;
        } else {
            throw new ResourceNotFoundException("resource with id " + transfertId + " not found");
        }
        return transfert;

    }

}