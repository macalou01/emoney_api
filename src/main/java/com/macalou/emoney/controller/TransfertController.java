package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Transfert;
import com.macalou.emoney.service.TransfertService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/transferts")
public class TransfertController {
    public final TransfertService transfertService;

    @Autowired
    public TransfertController(TransfertService transfertService) {
        this.transfertService = transfertService;
    }

    // **Get All */
    @GetMapping
    public List<Transfert> getTransferts() {
        return transfertService.findALL();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Transfert getOne(@PathVariable("id") Long id) {
        return transfertService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewTransfert(@RequestBody Transfert transfert) {
        transfertService.add(transfert);
    }

}