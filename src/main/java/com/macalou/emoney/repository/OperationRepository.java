package com.macalou.emoney.repository;

import com.macalou.emoney.model.Operation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface OperationRepository extends JpaRepository<Operation, Long> {

}