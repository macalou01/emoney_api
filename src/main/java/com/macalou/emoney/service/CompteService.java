package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Compte;
import com.macalou.emoney.repository.CompteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompteService {
    private final CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public void add(Compte compte) {
        compteRepository.save(compte);
    }

    public List<Compte> findAll() {
        List<Compte> comptes = new ArrayList<>();
        comptes = compteRepository.findAll();
        if (comptes.isEmpty()) {
            throw new ResourceNotFoundException("resouce not found");
        }
        return comptes;
    }

    public Compte findOne(Long compteId) {
        Compte compte = compteRepository.findById(compteId)
                .orElseThrow(() -> new ResourceNotFoundException("resource with id " + compteId + " not found"));
        return compte;
    }

    @Transactional
    public void updateCompte(Long compteId, Compte newCompte) {

        Compte compte = compteRepository.findById(compteId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + compteId + " not found"));
        compte.setSolde(newCompte.getSolde());
    }

    public void deleteCompte(Long compteId) {
        boolean exits = compteRepository.existsById(compteId);
        if (!exits) {
            throw new ResourceNotFoundException("resource with id " + compteId + " not found");
        }
        compteRepository.deleteById(compteId);

    }
}