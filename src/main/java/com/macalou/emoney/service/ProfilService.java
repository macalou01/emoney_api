package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Profil;
import com.macalou.emoney.repository.ProfilRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProfilService {
    private final ProfilRepository profilRepository;

    @Autowired
    public ProfilService(ProfilRepository profilRepository) {
        this.profilRepository = profilRepository;
    }

    public void add(Profil profil) {
        profilRepository.save(profil);
    }

    public List<Profil> findAll() {
        List<Profil> profils = new ArrayList<>();
        profils = profilRepository.findAll();
        if (profils.isEmpty()) {
            throw new ResourceNotFoundException("resouce not found");
        }
        return profils;
    }

    public Profil findOne(int profilId) {
        Profil profil = profilRepository.findById(profilId)
                .orElseThrow(() -> new ResourceNotFoundException("resource with id " + profilId + " not found"));
        return profil;
    }

    @Transactional
    public void updateProfil(int profilId, Profil newProfil) {

        Profil profil = profilRepository.findById(profilId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + profilId + " not found"));
        profil.setNomProfil(newProfil.getNomProfil());
        profil.setProfilRoles(newProfil.getProfilRoles());
    }

    public void deleteProfil(int profilId) {
        boolean exits = profilRepository.existsById(profilId);
        if (!exits) {
            throw new ResourceNotFoundException("resource with id " + profilId + " not found");
        }
        profilRepository.deleteById(profilId);

    }
}
