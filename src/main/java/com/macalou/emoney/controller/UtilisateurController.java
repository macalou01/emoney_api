package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Utilisateur;
import com.macalou.emoney.service.UtilisateurService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/utilisateurs")
public class UtilisateurController {
    public final UtilisateurService utilisateurService;

    @Autowired
    public UtilisateurController(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    // **Get All */
    @GetMapping
    public List<Utilisateur> getUtilisateurs() {
        return utilisateurService.findAll();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Utilisateur getOne(@PathVariable("id") Long id) {
        return utilisateurService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewUtilisateur(@RequestBody Utilisateur utilisateur) {
        utilisateurService.add(utilisateur);
    }

    // **Update */
    @PutMapping(path = "{id}")
    public void updateUtilisateur(@PathVariable("id") Long id, @RequestBody Utilisateur newUtilisateur) {
        utilisateurService.updateUtilisateur(id, newUtilisateur);
    }

    // **Delete */
    @DeleteMapping(path = "{id}")
    public void deleteUtilisateur(@PathVariable("id") Long id) {
        utilisateurService.deleteUtilisateur(id);

    }

}
