package com.macalou.emoney.config;

import com.macalou.emoney.model.Role;
import com.macalou.emoney.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Configuration
public class RoleConfig {
    @Component
    @Order(1)
    public class RoleRunner implements CommandLineRunner {

        @Autowired
        private RoleRepository repository;

        @Override
        public void run(String... args) throws Exception {

            Role r1 = new Role();
            Role r2 = new Role();
            Role r3 = new Role();
            Role r4 = new Role();
            r1.setNomRole("ajouter utilisateur");
            r2.setNomRole("supprimer utilisateur");
            r3.setNomRole("faire une transaction");
            r4.setNomRole("valider une transaction");

            repository.save(r1);
            repository.save(r2);
            repository.save(r3);
            repository.save(r4);

        }

    }
}
