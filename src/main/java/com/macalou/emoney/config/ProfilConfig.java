package com.macalou.emoney.config;

import java.util.List;

import com.macalou.emoney.model.Profil;
import com.macalou.emoney.model.Role;
import com.macalou.emoney.repository.ProfilRepository;
import com.macalou.emoney.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Configuration
public class ProfilConfig {
    @Component
    @Order(2)
    public class RoleRunner implements CommandLineRunner {

        @Autowired
        private ProfilRepository repository;
        @Autowired
        private RoleRepository roleRepo;

        @Override
        public void run(String... args) throws Exception {
            repository.deleteAll();

            List<Role> roleAdmin = roleRepo.findAll();
            List<Role> roleDistributeur = roleRepo.findAll();
            List<Role> roleClient = roleRepo.findAll();
            roleDistributeur.remove(0);
            roleDistributeur.remove(0);
            roleClient.remove(0);
            roleClient.remove(0);
            roleClient.remove(0);

            Profil profil1 = new Profil();
            Profil profil2 = new Profil();
            Profil profil3 = new Profil();

            profil1.setNomProfil("administrateur");
            profil1.setProfilRoles(roleAdmin);
            profil2.setNomProfil("distributeur");
            profil2.setProfilRoles(roleDistributeur);
            profil3.setNomProfil("client");
            profil3.setProfilRoles(roleClient);
            repository.save(profil1);
            repository.save(profil2);
            repository.save(profil3);

        }

    }
}