package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Role;
import com.macalou.emoney.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/roles")
public class RoleController {
    public final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    // **Get All */
    @GetMapping
    public List<Role> getRoles() {
        return roleService.findAll();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Role getOne(@PathVariable("id") int id) {
        return roleService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewRole(@RequestBody Role role) {
        roleService.add(role);
    }

    // **Update */
    @PutMapping(path = "{id}")
    public void updateRole(@PathVariable("id") int id, @RequestBody Role newRole) {
        roleService.updateRole(id, newRole);
    }

    // **Delete */
    @DeleteMapping(path = "{id}")
    public void deleteRole(@PathVariable("id") int id) {
        roleService.deleteRole(id);

    }

}
