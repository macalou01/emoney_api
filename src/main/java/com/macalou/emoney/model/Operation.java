package com.macalou.emoney.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type_operation")
@JsonSubTypes({
        @Type(value = Depot.class, name = "depot"),
        @Type(value = Retrait.class, name = "retrait"),
        @Type(value = Transfert.class, name = "transfert")
})
public abstract class Operation {

    @Id
    @SequenceGenerator(name = "operation_sequence", sequenceName = "operation_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operation_sequence")
    private long operationId;

    @NotNull
    private LocalDateTime dateOperation;

    @Min(value = 0L, message = "Le montant de l'opération doit être supperieur ou égal à 100")
    private long montant;

    @ManyToOne
    @JoinColumn(name = "fk_compteId", nullable = true) // TODO:set me false
    private Compte compte;

    @OneToMany(targetEntity = Historique.class, mappedBy = "operation", fetch = FetchType.LAZY)
    @JsonBackReference(value = "historique-operation")
    private List<Historique> historiques = new ArrayList<>();

    @Enumerated(value = EnumType.STRING)
    private TypeOperation typeOperation;

    public Operation() {
    }

    public Operation(long operationId, @NotNull LocalDateTime dateOperation, long montant, Compte compte,
            List<Historique> historiques, TypeOperation typeOperation) {
        this.operationId = operationId;
        this.dateOperation = dateOperation;
        this.montant = montant;
        this.compte = compte;
        this.historiques = historiques;
        this.typeOperation = typeOperation;
    }

    public Operation(@NotNull LocalDateTime dateOperation, long montant, Compte compte, List<Historique> historiques,
            TypeOperation typeOperation) {
        this.dateOperation = dateOperation;
        this.montant = montant;
        this.compte = compte;
        this.historiques = historiques;
        this.typeOperation = typeOperation;
    }

    public long getOperationId() {
        return operationId;
    }

    public void setOperationId(long operationId) {
        this.operationId = operationId;
    }

    public LocalDateTime getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(LocalDateTime dateOperation) {
        this.dateOperation = dateOperation;
    }

    public long getMontant() {
        return montant;
    }

    public void setMontant(long montant) {
        this.montant = montant;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public List<Historique> getHistoriques() {
        return historiques;
    }

    public void setHistoriques(List<Historique> historiques) {
        this.historiques = historiques;
    }

    public TypeOperation getTypeOperation() {
        return typeOperation;
    }

    public void setTypeOperation(TypeOperation typeOperation) {
        this.typeOperation = typeOperation;
    }

    @Override
    public String toString() {
        return "Operation [dateOperation=" + dateOperation + ", montant=" + montant + ", operationId=" + operationId
                + ", typeOperation=" + typeOperation + "]";
    }

}
