package com.macalou.emoney.service;

import java.util.List;

import javax.transaction.Transactional;

import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Historique;
import com.macalou.emoney.repository.HistoriqueRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoriqueService {
    private final HistoriqueRepository historiqueRepository;

    @Autowired
    public HistoriqueService(HistoriqueRepository historiqueRepository) {
        this.historiqueRepository = historiqueRepository;
    }

    public void add(Historique historique) {
        historiqueRepository.save(historique);
    }

    public List<Historique> findALL() {
        List<Historique> historiques = historiqueRepository.findAll();
        if (historiques.isEmpty()) {
            throw new ResourceNotFoundException("resource not found");
        }
        return historiques;
    }

    public Historique findOne(long historiqueId) {
        Historique historique = historiqueRepository.findById(historiqueId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + historiqueId + " not found"));
        return historique;

    }

    @Transactional
    public void updateHistorique(long historiqueId, Historique newHistorique) {

        Historique historique = historiqueRepository.findById(historiqueId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + historiqueId + " not found"));
        historique.setDate(newHistorique.getDate());
        historique.setEtat(newHistorique.getEtat());
    }

    public void deleteHistorique(long historiqueId) {
        boolean exits = historiqueRepository.existsById(historiqueId);
        if (!exits) {
            throw new ResourceNotFoundException("resoource with id " + historiqueId + " not found");
        }
        historiqueRepository.deleteById(historiqueId);
    }

}