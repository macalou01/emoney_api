package com.macalou.emoney.model;

public enum Sexe {
    Masculin(1),
    Feminin(2);

    final int code;

    Sexe(int code) {
        this.code = code;
    }
}
