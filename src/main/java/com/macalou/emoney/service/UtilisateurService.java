package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.BadRequestException;
import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Compte;
import com.macalou.emoney.model.Utilisateur;
import com.macalou.emoney.repository.CompteRepository;
import com.macalou.emoney.repository.UtilisateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UtilisateurService {
    private final UtilisateurRepository utilisateurRepository;
    private final CompteRepository compteRepository;

    @Autowired
    public UtilisateurService(UtilisateurRepository utilisateurRepository,CompteRepository compteRepository) {
        this.utilisateurRepository = utilisateurRepository;
        this.compteRepository=compteRepository;

    }

    public void add(Utilisateur utilisateur) {
        if(utilisateur.getCompte()==null){
            throw new BadRequestException("Vérifiez votre requête !");
        }
        Compte compte=new Compte();
        compteRepository.save(compte);
        utilisateur.setCompte(compte);
        utilisateurRepository.save(utilisateur);
    }

    public List<Utilisateur> findAll() {
        List<Utilisateur> utilisateur = new ArrayList<>();
        utilisateur = utilisateurRepository.findAll();
        if (utilisateur.isEmpty()) {
            throw new ResourceNotFoundException("resouce not found");
        }
        return utilisateur;
    }

    public Utilisateur findOne(Long utilisateurId) {
        Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId)
                .orElseThrow(() -> new ResourceNotFoundException("resource with id " + utilisateurId + " not found"));
        return utilisateur;
    }

    @Transactional
    public void updateUtilisateur(Long utilisateurId, Utilisateur newUtilisateur) {
        // TODO:find utilisateur by numeroTel
        Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + utilisateurId + " not found"));

        utilisateur.setMdp(newUtilisateur.getMdp());
        utilisateur.setEmail(newUtilisateur.getEmail());
        utilisateur.setNom(newUtilisateur.getNom());
        utilisateur.setPrenom(newUtilisateur.getPrenom());
        utilisateur.setNumeroTel(newUtilisateur.getNumeroTel());
        utilisateur.setSexe(newUtilisateur.getSexe());
        utilisateur.setDateNais(newUtilisateur.getDateNais());
        utilisateur.setProfil(newUtilisateur.getProfil());
    }

    public void deleteUtilisateur(Long utilisateurId) {
        boolean exits = utilisateurRepository.existsById(utilisateurId);
        if (!exits) {
            throw new ResourceNotFoundException("resource with id " + utilisateurId + " not found");
        }
        utilisateurRepository.deleteById(utilisateurId);

    }

}
