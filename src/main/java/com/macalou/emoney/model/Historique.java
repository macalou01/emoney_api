package com.macalou.emoney.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Historique {

    @Id
    @SequenceGenerator(name = "historique_sequence", sequenceName = "historique_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "historique_sequence")
    private long historiqueId;

    @NotNull
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "fk_utilisateurId", nullable = false)
    private Utilisateur effectuerPar;

    private String etat;

    @ManyToOne
    @JoinColumn(name = "fk_operationId", nullable = false)
    @JsonManagedReference(value = "historique-operation")
    @JsonIgnoreProperties("historiques")
    private Operation operation;

    public Historique() {
    }

    public Historique(long historiqueId, @NotNull LocalDateTime date, Utilisateur effectuerPar, String etat,
            Operation operation) {
        this.historiqueId = historiqueId;
        this.date = date;
        this.effectuerPar = effectuerPar;
        this.etat = etat;
        this.operation = operation;
    }

    public Historique(@NotNull LocalDateTime date, Utilisateur effectuerPar, String etat, Operation operation) {
        this.date = date;
        this.effectuerPar = effectuerPar;
        this.etat = etat;
        this.operation = operation;
    }

    public long getHistoriqueId() {
        return historiqueId;
    }

    public void setHistoriqueId(long historiqueId) {
        this.historiqueId = historiqueId;
    }

    public Utilisateur getEffectuerPar() {
        return effectuerPar;
    }

    public void setEffectuerPar(Utilisateur effectuerPar) {
        this.effectuerPar = effectuerPar;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "Historique [date=" + date + ", etat=" + etat + ", historiqueId=" + historiqueId + "]";
    }

}
