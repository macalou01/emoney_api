package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Depot;
import com.macalou.emoney.service.DepotService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/depots")
public class DepotController {
   public final DepotService depotService;

    @Autowired
    public DepotController(DepotService depotService) {
        this.depotService = depotService;
    }

    // **Get All */
    @GetMapping
    public List<Depot> getDepots() {
        return depotService.findALL();
    }

    // **Get One */
  @GetMapping(path = "{id}")
    public Depot getOne(@PathVariable("id") Long id) {
        return depotService.findOne(id);
    }

   // **Post 
    @PostMapping
    public void addNewDepot(@RequestBody Depot Depot) {
        depotService.add(Depot);
    }

}