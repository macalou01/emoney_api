package com.macalou.emoney.config;

import java.util.List;

import com.macalou.emoney.model.Compte;
import com.macalou.emoney.repository.CompteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Configuration
public class CompteConfig {
    @Component
    @Order(3)
    public class CompteRunner implements CommandLineRunner {
        @Autowired
        private CompteRepository compteRepo;

        @Override
        public void run(String... args) throws Exception {
            Compte compte1 = new Compte(1,5000000,null,null);
            Compte compte2 = new Compte(2,0,null,null);
            Compte compte3 = new Compte();
            compteRepo.saveAll(List.of(compte1, compte2, compte3));

        }

    }
}
