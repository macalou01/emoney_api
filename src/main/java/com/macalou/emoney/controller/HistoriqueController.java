package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Historique;
import com.macalou.emoney.service.HistoriqueService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/historiques")
public class HistoriqueController {
    public final HistoriqueService historiqueService;

    @Autowired
    public HistoriqueController(HistoriqueService historiqueService) {
        this.historiqueService = historiqueService;
    }

    // **Get All */
    @GetMapping
    public List<Historique> getHistoriques() {
        return historiqueService.findALL();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Historique getOne(@PathVariable("id") Long id) {
        return historiqueService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewHistorique(@RequestBody Historique historique) {
        historiqueService.add(historique);
    }

    // **Update */
    @PutMapping(path = "{id}")
    public void updateHistorique(@PathVariable("id") Long historiqueId, @RequestBody Historique newHistorique) {
        historiqueService.updateHistorique(historiqueId, newHistorique);
    }

    // **Delete */
    @DeleteMapping(path = "{id}")
    public void deleteHistorique(@PathVariable("id") Long id) {
        historiqueService.deleteHistorique(id);

    }

}