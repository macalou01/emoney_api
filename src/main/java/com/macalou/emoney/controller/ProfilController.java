package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Profil;
import com.macalou.emoney.service.ProfilService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="api/profils")
public class ProfilController {
    public final ProfilService profilService;
    
    @Autowired
    public ProfilController(ProfilService profilService) {
        this.profilService = profilService;
    }

    // **Get All */
    @GetMapping
    public List<Profil> getProfils() {
        return profilService.findAll();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Profil getOne(@PathVariable("id") int id) {
        return profilService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewProfil(@RequestBody Profil profil) {
        profilService.add(profil);
    }

    // **Update */
    @PutMapping(path = "{id}")
    public void updateProfil(@PathVariable("id") int id, @RequestBody Profil newProfil) {
       profilService.updateProfil(id, newProfil);
    }

    // **Delete */
    @DeleteMapping(path = "{id}")
    public void deleteprofil(@PathVariable("id") int id) {
      profilService.deleteProfil(id);

    }

}

