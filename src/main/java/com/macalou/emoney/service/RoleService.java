package com.macalou.emoney.service;

import java.util.ArrayList;
import java.util.List;

import com.macalou.emoney.exceptions.ResourceNotFoundException;
import com.macalou.emoney.model.Role;
import com.macalou.emoney.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void add(Role role) {

        roleRepository.save(role);
    }

    public List<Role> findAll() {
        List<Role> roles = new ArrayList<>();
        roles = roleRepository.findAll();
        if (roles.isEmpty()) {
            throw new ResourceNotFoundException("resouce not found");
        }
        return roles;
    }

    public Role findOne(int roleId) {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new ResourceNotFoundException("resource with id " + roleId + " not found"));
        return role;
    }

    @Transactional
    public void updateRole(int roleId, Role newRole) {

        Role role = roleRepository.findById(roleId).orElseThrow(
                () -> new ResourceNotFoundException("resource with id " + roleId + " not found"));
        role.setNomRole(newRole.getNomRole());
    }

    public void deleteRole(int roleId) {
        boolean exits = roleRepository.existsById(roleId);
        if (!exits) {
            throw new ResourceNotFoundException("resource with id " + roleId + " not found");
        }
        roleRepository.deleteById(roleId);

    }
}
