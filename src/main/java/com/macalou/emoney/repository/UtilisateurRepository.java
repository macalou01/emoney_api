package com.macalou.emoney.repository;

import com.macalou.emoney.model.Utilisateur;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
    //TODO:find by numTel
}
