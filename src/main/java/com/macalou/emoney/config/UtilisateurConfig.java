package com.macalou.emoney.config;

import java.time.LocalDate;
import java.time.Month;

import com.macalou.emoney.model.Compte;
import com.macalou.emoney.model.Profil;
import com.macalou.emoney.model.Sexe;
import com.macalou.emoney.model.Utilisateur;
import com.macalou.emoney.repository.CompteRepository;
import com.macalou.emoney.repository.ProfilRepository;
import com.macalou.emoney.repository.UtilisateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Configuration
public class UtilisateurConfig {
    @Component
    @Order(4)
    public class UtilisateurRunner implements CommandLineRunner {

        @Autowired
        private UtilisateurRepository repository;
        @Autowired
        private ProfilRepository profilRepo;
        @Autowired
        private CompteRepository compteRepo;

        @Override
        public void run(String... args) throws Exception {

            Profil profil1 = profilRepo.findAll().get(0);
            Profil profil2 = profilRepo.findAll().get(1);
            Profil profil3 = profilRepo.findAll().get(2);

            profil1.setProfilRoles(null);
            profil2.setProfilRoles(null);
            profil3.setProfilRoles(null);

            Compte compte1 = compteRepo.findAll().get(0);
            Compte compte2 = compteRepo.findAll().get(1);
            Compte compte3 = compteRepo.findAll().get(2);

            repository.save(new Utilisateur("77407702", "1234", "mody", "macalou", "macalou01@gmail.com", Sexe.Masculin,
                    LocalDate.of(1994, Month.MAY, 10), profil1, compte1,null));
            repository.save(new Utilisateur("5588569", "8787", "aida", "samake", "tidiane01@gmail.com", Sexe.Feminin,
                    LocalDate.of(1996, Month.DECEMBER, 12), profil2, compte2,null));
            repository.save(new Utilisateur("5489566", "5682", "youssouf", "diabate", "diabate01@gmail.com", Sexe.Feminin,
                    LocalDate.of(1984, Month.MAY, 10), profil3, compte3,null));


        }

    }
}
