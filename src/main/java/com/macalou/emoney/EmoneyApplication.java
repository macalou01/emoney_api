package com.macalou.emoney;

import java.time.LocalDateTime;

import com.macalou.emoney.model.Compte;
import com.macalou.emoney.model.Depot;
import com.macalou.emoney.model.Historique;
import com.macalou.emoney.model.Operation;
import com.macalou.emoney.model.Retrait;
import com.macalou.emoney.model.Transfert;
import com.macalou.emoney.model.TypeOperation;
import com.macalou.emoney.model.Utilisateur;
import com.macalou.emoney.repository.CompteRepository;
import com.macalou.emoney.repository.DepotRepository;
import com.macalou.emoney.repository.HistoriqueRepository;
import com.macalou.emoney.repository.OperationRepository;
import com.macalou.emoney.repository.ProfilRepository;
import com.macalou.emoney.repository.RetraitRepository;
import com.macalou.emoney.repository.TransfertRepository;
import com.macalou.emoney.repository.UtilisateurRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class EmoneyApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(EmoneyApplication.class,
				args);
		OperationRepository retraitRepository = configurableApplicationContext.getBean(RetraitRepository.class);
		OperationRepository depotRepository = configurableApplicationContext.getBean(DepotRepository.class);
		OperationRepository transfertRepository = configurableApplicationContext.getBean(TransfertRepository.class);
		HistoriqueRepository historiqueRepository = configurableApplicationContext.getBean(HistoriqueRepository.class);
		UtilisateurRepository utilisateurRepository = configurableApplicationContext
				.getBean(UtilisateurRepository.class);
		CompteRepository compteRepository = configurableApplicationContext.getBean(CompteRepository.class);
		ProfilRepository profilRepository=configurableApplicationContext.getBean(ProfilRepository.class);
		Operation depot = new Depot(LocalDateTime.now(), 5000000, null, null, TypeOperation.DEPOT, 77407702L);
		Operation retrait = new Retrait(LocalDateTime.now(), 1000000, null, null, TypeOperation.RETRAIT, 79316604L);
		Operation transfert = new Transfert(LocalDateTime.now(), 1000000, null, null, TypeOperation.TRANSFERT,
				70000000L);
		retraitRepository.save(retrait);
		depotRepository.save(depot);
		transfertRepository.save(transfert);

		Historique historique = new Historique(LocalDateTime.now(), utilisateurRepository.findById(1L).get(), "ok",
				depotRepository.findById(1L).get());
		historiqueRepository.save(historique);
		Compte compte = new Compte();
		Utilisateur utilisateur = new Utilisateur();

		utilisateur.setNumeroTel("65518186");
		utilisateur.setCompte(compte);
		utilisateur.setProfil(profilRepository.findById(3).get());
		compte.setSolde(10000000);
		compteRepository.save(compte);
		utilisateur.setCompte(compte);
		utilisateurRepository.save(utilisateur);
		// TODO:getALLOperation(operation controleur)
	}

}
