package com.macalou.emoney.controller;

import java.util.List;

import com.macalou.emoney.model.Compte;
import com.macalou.emoney.service.CompteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/comptes")
public class CompteController {
    public final CompteService compteService;

    @Autowired
    public CompteController(CompteService compteService) {
        this.compteService = compteService;
    }

    // **Get All */
    @GetMapping
    public List<Compte> getComptes() {
        return compteService.findAll();
    }

    // **Get One */
    @GetMapping(path = "{id}")
    public Compte getOne(@PathVariable("id") Long id) {
        return compteService.findOne(id);
    }

    // **Post */
    @PostMapping
    public void addNewCompte(@RequestBody Compte compte) {
        compteService.add(compte);
    }

    // **Update */
    @PutMapping(path = "{id}")
    public void updateCompte(@PathVariable("id") Long id, @RequestBody Compte newCompte) {
        compteService.updateCompte(id, newCompte);
    }

    // **Delete */
    @DeleteMapping(path = "{id}")
    public void deleteCompte(@PathVariable("id") Long id) {
        compteService.deleteCompte(id);

    }

}